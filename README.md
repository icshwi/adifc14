ADIFC14
=========

areaDetector driver for IFC14xx systems. Will eventually replace NDS3-based support for this kind of devices.

Based on [ADSIS8300](https://gitlab.esss.lu.se/beam-diagnostics/bde/epics/modules/adsis8300) by Hinko Kocevar

