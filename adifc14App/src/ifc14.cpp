/* ifc14.h
 *
 * This is a driver for a IOxOS IFC1410 board with ADC311X digitizers FMCs.
 * Based on ADSIS8300 from Hinko Kocevar (ESS ERIC).
 *
 * Author: Joao Paulo Martins
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  January 03, 2019
 *
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <string>

#include <dbAccess.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <iocsh.h>

#include <asynNDArrayDriver.h>
#include <epicsExport.h>

#include "ifcdaqdrv.h"
#include "ifc14.h"

static const char *driverName = "adifc14";

/**
 * Exit handler, delete the ifc1400 object.
 */
static void exitHandler(void *drvPvt) {
    ifc1400 *pPvt = (ifc1400 *) drvPvt;
    delete pPvt;
}

static void ifcTaskC(void *drvPvt)
{
    ifc1400 *pPvt = (ifc1400 *)drvPvt;
    pPvt->ifcTask();
}

/*
 * Round upwards to nearest power-of-2.
 */

static inline int ceil_pow2(unsigned number) {
    unsigned n = 1;
    unsigned i = number - 1;
    while(i) {
        n <<= 1;
        i >>= 1;
    }
    return n;
}

/*
 * Round upwards to nearest mibibytes
 */

static inline int ceil_mibi(unsigned number) {
    return (1 + (number-1) / (1024 * 1024)) * 1024 * 1024;
}

// user-defined time stamp source callback invoked on updateTimeStamp() call
static void evrTimeStampSource(void *drvPvt, epicsTimeStamp *pTimeStamp)
{
    ifc1400 *pPvt = (ifc1400 *)drvPvt;

    // if TimestampLink.DOL PV value is not set, time stamp will be 0.0
    // if TimestampLink.DOL PV is set and then unset, Timestamp PV should
    // be set to 0.0 manually to make this work
    if (pPvt->mTimeStampSec && pPvt->mTimeStampNsec) {
        // time stamp obtained from the EVR
        pTimeStamp->secPastEpoch = pPvt->mTimeStampSec;
        pTimeStamp->nsec = pPvt->mTimeStampNsec;
    } else {
        // default time stamp source
        epicsTimeGetCurrent(pTimeStamp);
    }
}


/** Constructor for ifc1400; most parameters are simply passed to asynNDArrayDriver::asynNDArrayDriver.
  * After calling the base class constructor this method creates a thread to compute the simulated detector data,
  * and sets reasonable default values for parameters defined in this class, asynNDArrayDriver and ADDriver.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] ifccard Identifier handler for the IFC14xx card.
  * \param[in] maxChannels Max number of ADC channels to be acquired.
  * \param[in] numSamples The initial number of AI samples.
  * \param[in] extraPorts Additinonal number of asyn ports to register. Used for class that extend this class.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
ifc1400::ifc1400(const char *portName, const int ifccard, const int maxChannels,
            int numSamples, int extraPorts, int maxBuffers, size_t maxMemory,
            int priority, int stackSize)

    : asynNDArrayDriver(portName,
            maxChannels + extraPorts,
            maxBuffers, maxMemory,
            asynFloat64ArrayMask | asynUInt32DigitalMask,
            asynFloat64ArrayMask | asynUInt32DigitalMask,
            ASYN_CANBLOCK | ASYN_MULTIDEVICE, /* asyn flags*/
            1,                                /* autoConnect=1 */
            priority,
            stackSize),
            mUniqueId(0), mAcquiring(0)

{

    int status = asynSuccess;
    asynPrintDeviceInfo(pasynUserSelf, "we have %d asyn addresses", maxAddr);

    mRawDataArray = NULL;
    mNumArrays = maxChannels;
    mNumChannels = maxChannels;

    /* Create an EPICS exit handler */
    epicsAtExit(exitHandler, this);

    /* Creation of ifcdaqdrv2 handler */
    mIFCDevice = (struct ifcdaqdrv_usr*)calloc(1, sizeof(struct ifcdaqdrv_usr));
    mIFCDevice->card = (uint32_t) ifccard;
    mIFCDevice->fmc = 1;
    mIFCDevice->device = 0;

    /* Creation of ifcdaqdrv2 handler (FMC2) */
    mIFCDevice2 = (struct ifcdaqdrv_usr*)calloc(1, sizeof(struct ifcdaqdrv_usr));
    mIFCDevice2->card = (uint32_t) ifccard;
    mIFCDevice2->fmc = 2;
    mIFCDevice2->device = 0;

    // create the epicsEvents for signaling to the acquisition
    // task when acquisition starts and stops
    this->mStartEventId = epicsEventCreate(epicsEventEmpty);
    if (! this->mStartEventId) {
        asynPrintError(pasynUserSelf, "epicsEventCreate failure for start event");
    }
    this->mStopEventId = epicsEventCreate(epicsEventEmpty);
    if (! this->mStopEventId) {
        asynPrintError(pasynUserSelf, "epicsEventCreate failure for stop event");
    }

    // system parameters
    // system parameters
    createParam(IFCNumSamplesString,                    asynParamInt32,     &IFCNumSamples);
    createParam(IFCClockSourceString,                   asynParamInt32,     &IFCClockSource);
    createParam(IFCClockDividerString,                  asynParamInt32,     &IFCClockDivider);
    createParam(IFCTriggerSourceString,                 asynParamInt32,     &IFCTriggerSource);
    createParam(IFCTriggerLineString,                   asynParamInt32,     &IFCTriggerExternalLine);
    createParam(IFCTriggerDelayString,                  asynParamInt32,     &IFCTriggerDelay);
    createParam(IFCTriggerRepeatString,                 asynParamInt32,     &IFCTriggerRepeat);
    createParam(IFCIrqPollString,                       asynParamInt32,     &IFCIrqPoll);
    createParam(IFCResetString,                         asynParamInt32,     &IFCReset);
    createParam(IFCDriverMessageString,                 asynParamOctet,     &IFCDriverMessage);
    createParam(IFCMemorySizeString,                    asynParamInt32,     &IFCMemorySize);
    createParam(IFCDevicePathString,                    asynParamOctet,     &IFCDevicePath);
    createParam(IFCRtmTypeString,                       asynParamInt32,     &IFCRtmType);
    createParam(IFCSamplingFrequencyString,             asynParamFloat64,   &IFCSamplingFrequency);
    createParam(IFCTickValueString,                     asynParamFloat64,   &IFCTickValue);
    createParam(IFCBeamModeSourceString,                asynParamInt32,     &IFCBeamModeSource);
    createParam(IFCBeamModeValueString,                 asynParamInt32,     &IFCBeamModeValue);
    createParam(IFCBeamDestinationSourceString,         asynParamInt32,     &IFCBeamDestinationSource);
    createParam(IFCBeamDestinationValueString,          asynParamInt32,     &IFCBeamDestinationValue);
    createParam(IFCPcieLinkSpeedString,                 asynParamInt32,     &IFCPcieLinkSpeed);
    createParam(IFCPcieLinkWidthString,                 asynParamInt32,     &IFCPcieLinkWidth);
    createParam(IFCPcieTransferTimeString,              asynParamFloat64,   &IFCPcieTransferTime);
    createParam(IFCPcieStartTimeString,                 asynParamFloat64,   &IFCPcieStartTime);
    createParam(IFCPcieEndTimeString,                   asynParamFloat64,   &IFCPcieEndTime);
    createParam(IFCTransferArraysString,                asynParamInt32,     &IFCTransferArrays);
    createParam(IFCTriggerTimeoutString,                asynParamInt32,     &IFCTriggerTimeout);
    createParam(IFCTriggerTimeoutValueString,           asynParamInt32,     &IFCTriggerTimeoutValue);
    createParam(IFCAPPSinatureString,                   asynParamInt32,     &IFCAPPSinature);
    createParam(IFCFWSignatureString,                   asynParamInt32,     &IFCFWSignature);
    createParam(IFCDeviceTypeString,                    asynParamInt32,     &IFCDeviceType);
    createParam(IFCDeviceTypeStrString,                 asynParamOctet,     &IFCDeviceTypeStr);

    // EVR parameters
    createParam(IFCEvrTimestampString,                  asynParamOctet,     &IFCEvrTimestamp);
    createParam(IFCEvrBeamStateString,                  asynParamInt32,     &IFCEvrBeamState);
    createParam(IFCEvrLinkString,                       asynParamInt32,     &IFCEvrLink);
    createParam(IFCEvrLinkStatusString,                 asynParamInt32,     &IFCEvrLinkStatus);
    createParam(IFCEvrLinkSeverityString,               asynParamInt32,     &IFCEvrLinkSeverity);
    // channel parameters
    createParam(IFCChControlString,                     asynParamInt32,     &IFCChControl);
    createParam(IFCChAttenuationString,                 asynParamFloat64,   &IFCChAttenuation);
    createParam(IFCChInternalTriggerLengthString,       asynParamInt32,     &IFCChInternalTriggerLength);
    createParam(IFCChInternalTriggerConditionString,    asynParamInt32,     &IFCChInternalTriggerCondition);
    createParam(IFCChInternalTriggerOffString,          asynParamInt32,     &IFCChInternalTriggerOff);
    createParam(IFCChInternalTriggerOnString,           asynParamInt32,     &IFCChInternalTriggerOn);
    createParam(IFCChPcieTransferTimeString,            asynParamFloat64,   &IFCChPcieTransferTime);
    createParam(IFCChPcieStartTimeString,               asynParamFloat64,   &IFCChPcieStartTime);
    createParam(IFCChPosInputString,                    asynParamInt32,     &IFCChPosInput);
    createParam(IFCChNegInputString,                    asynParamInt32,     &IFCChNegInput);
    createParam(IFCChGainString,                        asynParamInt32,     &IFCChGain);

    // this is digitizer native sample format
    setIntegerParam(NDDataType, NDInt32);

    setStringParam(ADManufacturer, "IOxOS");
    setStringParam(IFCDevicePath, std::to_string(ifccard).c_str());
    setStringParam(IFCEvrTimestamp, "");

    // register our time stamp source function that will set time stamp obtained
    // from EVR
    pasynManager->registerTimeStampSource(pasynUserSelf, this, evrTimeStampSource);

    /* Create the thread that updates the data */
    status = (epicsThreadCreate("IFCTask",
                                epicsThreadPriorityHigh,
                                epicsThreadGetStackSize(epicsThreadStackMedium),
                                (EPICSTHREADFUNC)ifcTaskC,
                                this) == NULL);
    if (status) {
        asynPrintError(pasynUserSelf, "epicsThreadCreate failure for acquisition task");
    }

    int ret = initDevice();
    if (ret) {
        asynPrintError(pasynUserSelf, "initialization FAILED");
        setStringParam(IFCDriverMessage, "initialization FAILED");
    } else {
        asynPrintDeviceInfo(pasynUserSelf, "initialization PASSED");
        setStringParam(IFCDriverMessage, "initialization PASSED");
    }
}

ifc1400::~ifc1400() {
    asynPrintDeviceInfo(pasynUserSelf, "Shutdown and freeing up memory...");

    this->lock();
    asynPrintDeviceInfo(pasynUserSelf, "Data thread is already down!");
    destroyDevice();
    free(mIFCDevice);
    free(mIFCDevice2);
    this->unlock();
    asynPrintDeviceInfo(pasynUserSelf, "Shutdown complete!");
}

int ifc1400::initDevice()
{
    unsigned int deviceType = 0x3117;
    unsigned int APPSinature = 0x00;
    unsigned int FWSignature = 0x00;
    char deviceTypeStr[256];

    ifcdaqdrv_status ret;

    /* Opening FMC1*/
    ret = ifcdaqdrv_open_device(mIFCDevice);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_open_device returned %d", ret);
        return (int)ret;
    }

    ret = ifcdaqdrv_init_adc(mIFCDevice);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_init_adc returned %d", ret);
        return (int)ret;
    }

    /* Opening FMC2*/
    ret = ifcdaqdrv_open_device(mIFCDevice2);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_open_device returned %d", ret);
        return (int)ret;
    }

    ret = ifcdaqdrv_init_adc(mIFCDevice2);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_init_adc returned %d", ret);
        return (int)ret;
    }

    /* Reading hardware details */
    ret = ifcdaqdrv_get_fw_version(mIFCDevice, (uint8_t*) &APPSinature);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_fw_version returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_get_fw_revision(mIFCDevice, (uint8_t*) &FWSignature);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_fw_revision returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_get_product_name(mIFCDevice, deviceTypeStr, 256);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_product_name returned %d", ret);
        return (int)ret;
    }

    setIntegerParam(IFCAPPSinature, APPSinature);
    setIntegerParam(IFCFWSignature, FWSignature);
    setIntegerParam(IFCDeviceType, deviceType);
    setStringParam(IFCDeviceTypeStr, deviceTypeStr);

    /* Get sampling rate and clock parameters */
    int pAveraging;
    int pDecimation;
    int pClkDiv;
    double pClkFreq;
    int clkSource;
    double pSamplingRate;

    ret = ifcdaqdrv_get_clock_source(mIFCDevice, (ifcdaqdrv_clock*) &clkSource);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_source returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_get_clock_divisor(mIFCDevice, (uint32_t*) &pClkDiv);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_divisor returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_set_sample_rate(mIFCDevice, 5000000.0);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_set_sample_rate returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_set_sample_rate(mIFCDevice2, 5000000.0);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_set_sample_rate returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_get_clock_frequency(mIFCDevice, &pClkFreq);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_frequency returned %d", ret);
        return (int)ret;
    }
    ret = ifcdaqdrv_calc_sample_rate(mIFCDevice, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 1);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_calc_sample_rate returned %d", ret);
        return (int)ret;
    }

    setIntegerParam(IFCClockDivider, pClkDiv);
    setDoubleParam(IFCSamplingFrequency, pSamplingRate);
    setIntegerParam(IFCClockSource, clkSource);

    //TODO: proper initalization of NUMBER of SAMPLES
    int numSamples_ = 2048;

    // maximum number of samples we can ask for, per channel
    uint32_t maxSamples;
    ret = ifcdaqdrv_get_nsamples(mIFCDevice, &maxSamples);
    if (numSamples_ > (int) maxSamples) 
        numSamples_ = maxSamples;
    setIntegerParam(IFCNumSamples, numSamples_);

    uint32_t nchannels;
  	ret = ifcdaqdrv_get_nchannels(mIFCDevice, &nchannels);
	if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_nchannels returned %d", ret);
        return (int)ret;
    }
    
    int memoryKb = ((maxSamples*sizeof(u_int16_t))*nchannels*2)/1024;
    setIntegerParam(IFCMemorySize, memoryKb);

    // first time read back the firmware register values
    int ret2;
    ret2 = readbackParameters();
    return ret2;
}

int ifc1400::destroyDevice()
{
    ifcdaqdrv_status ret;

    ret = ifcdaqdrv_close_device(mIFCDevice);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_close_device returned %d", ret);
        return (int)ret;
    }

    ret = ifcdaqdrv_close_device(mIFCDevice2);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_close_device returned %d", ret);
        return (int)ret;
    }
    return 0;
}

// perform (read/write) FPGA register readback *before* the acquisition start
// note that (read-only) FPGA register readout is performed *after* the acquisition ends
int ifc1400::readbackParameters()
{
    ifcdaqdrv_status ret;
    
    unsigned int uintValue;
    ret = ifcdaqdrv_get_nsamples(mIFCDevice, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_nsamples returned %d", ret);
        return (int)ret;
    }
    setIntegerParam(IFCNumSamples, uintValue);

    int clkSource;
    ret = ifcdaqdrv_get_clock_source(mIFCDevice, (ifcdaqdrv_clock*) &clkSource);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_source returned %d", ret);
        return (int)ret;
    }
    setIntegerParam(IFCClockSource, clkSource);


    int pClkDiv;
    //double pClkFreq;
    //double pSamplingRate;
    ret = ifcdaqdrv_get_clock_divisor(mIFCDevice, (uint32_t*) &pClkDiv);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_divisor returned %d", ret);
        return (int)ret;
    }
    setIntegerParam(IFCClockDivider, pClkDiv);

    // ret = sis8300drv_get_trigger_source(mDeviceHandle, (sis8300drv_trg_src *)&uintValue);
    // if (ret) {
    //     asynPrintError(pasynUserSelf, "sis8300drv_get_trigger_source returned %d", ret);
    //     return ret;
    // }
    // setIntegerParam(SIS8300TriggerSource, uintValue);

    //TODO: PCIe measurements
    setIntegerParam(IFCPcieLinkSpeed, 0);
    setIntegerParam(IFCPcieLinkWidth, 0);

    // XXX add missing readbacks here..
    callParamCallbacks();


    // Individual channel parameters
    //TODO: improve here
    for (int aich = 0; aich < 40; aich++) {
        double gaind_;
        if (aich < 20)
            ret = ifcdaqdrv_get_gain(mIFCDevice, aich, &gaind_);
        else
            ret = ifcdaqdrv_get_gain(mIFCDevice2, (aich-20), &gaind_);
		
        if (ret) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_gain returned %d", ret);
            return (int)ret;
        }

        setIntegerParam(aich, IFCChGain, static_cast<int>(gaind_));
    }

    uint8_t posinput, neginput;
    for (int aich = 0; aich < 40; aich++) {
        if (aich < 20)
            ret = ifcdaqdrv_get_adc_channel_positive_input(mIFCDevice, aich, &posinput);
        else
            ret = ifcdaqdrv_get_adc_channel_positive_input(mIFCDevice2, (aich-20), &posinput);
		
        if (ret) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_adc_channel_positive_input returned %d", ret);
            return (int)ret;
        }
        setIntegerParam(aich, IFCChPosInput, posinput);


        if (aich < 20)
            ret = ifcdaqdrv_get_adc_channel_negative_input(mIFCDevice, aich, &neginput);
        else
            ret = ifcdaqdrv_get_adc_channel_negative_input(mIFCDevice2, (aich-20), &neginput);
		
        if (ret) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_adc_channel_negative_input returned %d", ret);
            return (int)ret;
        }
        setIntegerParam(aich, IFCChNegInput, neginput);
        callParamCallbacks(aich);
    }
    return 0;
}

bool ifc1400::checkEvrLink()
{
    int evrLink;
    getIntegerParam(IFCEvrLink, &evrLink);
    // Link-Sts EVR PV value 1 means link is up
    if (evrLink != 1) {
        return false;
    }

    int evrLinkStatus;
    getIntegerParam(IFCEvrLinkStatus, &evrLinkStatus);
    // Link-Sts.STAT EVR PV value 0 means no alarms
    if (evrLinkStatus != 0) {
        return false;
    }

    int evrLinkSeverity;
    getIntegerParam(IFCEvrLinkSeverity, &evrLinkSeverity);
    // Link-Sts.SEVR EVR PV value 0 means no alarms
    if (evrLinkSeverity != 0) {
        return false;
    }

    return true;
}

int ifc1400::initDeviceDone()
{
    return 0;
}

int ifc1400::armDevice()
{
    ifcdaqdrv_status ret;
    ret = ifcdaqdrv_arm_device(mIFCDevice);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_arm_device returned %d", ret);
        return (int)ret;
    }
    return 0;
}

int ifc1400::disarmDevice()
{
    ifcdaqdrv_status ret;
    ret = ifcdaqdrv_disarm_device(mIFCDevice);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_disarm_device returned %d", ret);
        return (int)ret;
    }
    return 0;
}

int ifc1400::waitForDevice()
{
    ifcdaqdrv_status ret;
    ret = ifcdaqdrv_wait_acq_end(mIFCDevice);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_wait_acq_end returned %d", ret);
        return (int)ret;
    }
    return 0;
}

int ifc1400::deviceDone()
{
    // record the time of interrupt arrival (acquisition done)
    epicsTimeGetCurrent(&mInterruptTs);
    return 0;
}

int ifc1400::setupChannelMask(int addr)
{
    unsigned int channelMask;
    int state;
    getIntegerParam(addr, IFCChControl, &state);
    if (state) {
        channelMask |= (1 << addr);
    } else {
        channelMask &= ~(1 << addr);
    }
    return 0;
}

void ifc1400::updateTickValue()
{
    // Handle time axis time tick updates that depend on:
    //    - sampling frequency
    //    - clock source

    int clockSrc, clockDiv;
    double samplingFreq;
    getIntegerParam(IFCClockSource, &clockSrc);
    getIntegerParam(IFCClockDivider, &clockDiv);
    getDoubleParam(IFCSamplingFrequency, &samplingFreq);

    asynPrintDeviceInfo(pasynUserSelf, "using sampling frequency %g", samplingFreq);
    double tick = 1.0 / samplingFreq;
    setDoubleParam(IFCTickValue, tick);
    asynPrintDeviceInfo(pasynUserSelf, "time axis tick set to %g s", tick);
}

int ifc1400::setupInternalTrigger(int addr)
{
    return 0;
}

int ifc1400::acquireRawArrays()
{
    size_t dims[1]; // One NDArray of one dimension per channel
    int numAiSamples, samplesBuffer;
    ifcdaqdrv_status ret;

    epicsTimeStamp tNow, tEnd, tStart;
    double elapsed, total;
    
    uint32_t aich;
    int32_t *pRaw;
    int32_t *pRaw2;

    getIntegerParam(IFCNumSamples, &numAiSamples);
    dims[0] = numAiSamples;

    total = 0;
    // reset PCIe times
    for (aich = 0; aich < (mNumChannels/2); aich++) {
        setDoubleParam(aich, IFCChPcieTransferTime, 0.0);
        setDoubleParam(aich, IFCChPcieStartTime, 0.0);
    }
    setDoubleParam(IFCPcieTransferTime, 0.0);
    
    // record PCIe start time (delay after receiving the interrupt)
    epicsTimeGetCurrent(&tStart);
    elapsed = epicsTimeDiffInSeconds(&tStart, &mInterruptTs);
    setDoubleParam(IFCPcieStartTime, elapsed * 1000.0);
    setDoubleParam(IFCPcieEndTime, 0.0);
    int doTransfer;
    getIntegerParam(IFCTransferArrays, &doTransfer);

    /* FMC1 */
    /* Driver reads data according to the size of the buffer - prevent seg fault*/
    ret = ifcdaqdrv_get_nsamples(mIFCDevice, (uint32_t*) &samplesBuffer);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_nsamples returned %d", ret);
        return (int)ret;
    } 
    pRaw = (int32_t *)calloc(samplesBuffer, sizeof(int32_t));

    for (aich = 0; aich < (mNumChannels/2); aich++) {      
        /* Release the lock of the particular NDArray */
        if (this->pArrays[aich]) {
            this->pArrays[aich]->release();
        }
        this->pArrays[aich] = NULL;

        // check if we need to transfer the channel data
        if (! doTransfer) {
            continue;
        }      

        int chState;
        getIntegerParam(aich, IFCChControl, &chState);
        if (! chState) {
            continue;
        }

        // record PCIe start time of this channel
        epicsTimeGetCurrent(&tNow);
        elapsed = epicsTimeDiffInSeconds(&tNow, &tStart);
        setDoubleParam(aich, IFCChPcieStartTime, elapsed * 1000.0);

        /* Allocate NDArray */
        asynPrintDeviceInfo(pasynUserSelf, "AI addr %d, max samples %d", aich, numAiSamples);
        this->pArrays[aich] = pNDArrayPool->alloc(1, dims, NDInt32, 0, 0);
        epicsInt32 *pData = (epicsInt32 *)this->pArrays[aich]->pData;

        /* Read individual AI channels using temporary buffer pRaw */
        ret = ifcdaqdrv_read_ai_ch(mIFCDevice, aich, (int32_t*) pRaw);
        if (ret) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_read_ai_ch returned %d", ret);
            return (int)ret;
        } 
        //TODO: proper data conversion
        for (int samp = 0; samp < numAiSamples; samp++) {
            pData[samp] = static_cast<epicsInt32>(pRaw[samp]);
        }

        // record PCIe transfer time of this channel
        epicsTimeGetCurrent(&tEnd);
        elapsed = epicsTimeDiffInSeconds(&tEnd, &tNow);
        asynPrintDeviceInfo(pasynUserSelf, "channel %d, PCIe elapsed %f, total %f\n", aich, elapsed, total);
        setDoubleParam(aich, IFCChPcieTransferTime, elapsed * 1000.0);
        // update PCIe total time
        total += elapsed;
    }

    /* FMC2 */
    /* Driver reads data according to the size of the buffer - prevent seg fault*/
    ret = ifcdaqdrv_get_nsamples(mIFCDevice2, (uint32_t*) &samplesBuffer);
    if (ret) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_nsamples returned %d", ret);
        return (int)ret;
    } 
    pRaw2 = (int32_t *)calloc(samplesBuffer, sizeof(int32_t));

    for (aich = (mNumChannels/2); aich < mNumChannels; aich++) {      
        /* Release the lock of the particular NDArray */
        if (this->pArrays[aich]) {
            this->pArrays[aich]->release();
        }
        this->pArrays[aich] = NULL;

        // check if we need to transfer the channel data
        if (! doTransfer) {
            continue;
        }      

        int chState;
        getIntegerParam(aich, IFCChControl, &chState);
        if (! chState) {
            continue;
        }

        // record PCIe start time of this channel
        epicsTimeGetCurrent(&tNow);
        elapsed = epicsTimeDiffInSeconds(&tNow, &tStart);
        setDoubleParam(aich, IFCChPcieStartTime, elapsed * 1000.0);

        /* Allocate NDArray */
        asynPrintDeviceInfo(pasynUserSelf, "AI addr %d, max samples %d", aich, numAiSamples);
        this->pArrays[aich] = pNDArrayPool->alloc(1, dims, NDInt32, 0, 0);
        epicsInt32 *pData = (epicsInt32 *)this->pArrays[aich]->pData;

        /* Read individual AI channels using temporary buffer pRaw */
        ret = ifcdaqdrv_read_ai_ch(mIFCDevice2, (aich-(mNumChannels/2)), (int32_t*) pRaw2);
        if (ret) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_read_ai_ch returned %d", ret);
            return (int)ret;
        } 
        //TODO: proper data conversion
        for (int samp = 0; samp < numAiSamples; samp++) {
            pData[samp] = static_cast<epicsInt32>(pRaw2[samp]);
        }

        // record PCIe transfer time of this channel
        epicsTimeGetCurrent(&tEnd);
        elapsed = epicsTimeDiffInSeconds(&tEnd, &tNow);
        asynPrintDeviceInfo(pasynUserSelf, "channel %d, PCIe elapsed %f, total %f\n", aich, elapsed, total);
        setDoubleParam(aich, IFCChPcieTransferTime, elapsed * 1000.0);
        // update PCIe total time
        total += elapsed;
    }

    // record the PCIe end time
    epicsTimeGetCurrent(&tEnd);
    elapsed = epicsTimeDiffInSeconds(&tEnd, &mInterruptTs);
    setDoubleParam(IFCPcieEndTime, elapsed * 1000.0);

    // record the PCIe total time (sum of all channels)
    asynPrintDeviceInfo(pasynUserSelf, "PCIe total %f ms\n", total * 1000.0);
    setDoubleParam(IFCPcieTransferTime, total * 1000.0);
    if ((total * 1000.0) > 50.0) {
        asynPrintError(pasynUserSelf, "WARNING! PCIe total %f ms\n", total * 1000.0);
    }

    // free pRaw
    free(pRaw);
    free(pRaw2);
    
    return 0;
}

int ifc1400::acquireArrays()
{
    return acquireRawArrays();
}

void ifc1400::setAcquire(int value)
{
    if (value && !mAcquiring) {
        // send an event to start the acquisition
        epicsEventSignal(this->mStartEventId);
    }
    if (!value && mAcquiring) {
        // send an event to stop the acquisition
        epicsEventSignal(this->mStopEventId);
    }
}

void ifc1400::ifcTask()
{
    asynPrintDeviceInfo(pasynUserSelf, "waiting for iocInit to finish..");
    while (! interruptAccept) {
        epicsThreadSleep(0.1);
    }
    asynPrintDeviceInfo(pasynUserSelf, "data thread start");
    
    int status = asynSuccess;
    NDArray *pData;
    int arrayCounter;
    int triggerRepeat;
    int triggerCount = 0;
    int ret;
    int chState;
    int numChEnabled;


    this->lock();
    while (1) {

taskStart:
        // acquisition might have been stopped by the user
        status = epicsEventTryWait(this->mStopEventId);
        if (status == epicsEventWaitOK) {
            asynPrintDeviceInfo(pasynUserSelf, "stop event detected!");
            mAcquiring = 0;
        }

        // if we are not acquiring then wait
        if (! mAcquiring) {
            disarmDevice();
            setIntegerParam(ADAcquire, 0);

            for (int addr = 0; addr < maxAddr; addr++) {
                callParamCallbacks(addr);
            }

            asynPrintDeviceInfo(pasynUserSelf, "waiting for start event..");
            // release the lock while we wait for the start event
            this->unlock();
            status = epicsEventWait(this->mStartEventId);
            this->lock();
            asynPrintDeviceInfo(pasynUserSelf, "start event arrived!");

            snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "%s", "");
            setStringParam(IFCDriverMessage, mMessageBuffer);

            mAcquiring = 1;
            triggerCount = 0;

            ret = initDeviceDone();
            if (ret) {
                mAcquiring = 0;
                goto taskStart;
            }
        }

        //TODO: verify if device is open
        ret = 1;
        if (! ret) {
            asynPrintError(pasynUserSelf, "device %s not opened!", mDevicePath);
            mAcquiring = 0;
            goto taskStart;
        }

        // count enabled channels
        numChEnabled = 0;
        for (int a = 0; a < maxAddr; a++) {
            getIntegerParam(a, IFCChControl, &chState);
            if (chState) {
                numChEnabled++;
            }
        }

        // at least one channel needs to be enabled
        asynPrintDeviceInfo(pasynUserSelf, "Have %d channels enabled", numChEnabled);
        if (numChEnabled == 0) {
            asynPrintError(pasynUserSelf, "No channels enabled, stopping acquisition!");
            mAcquiring = 0;
            snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "No channels enabled, stopping acquisition!");
            setStringParam(IFCDriverMessage, mMessageBuffer);
            goto taskStart;
        }

        ret = armDevice();
        if (ret) {
            mAcquiring = 0;
            goto taskStart;
        }
        callParamCallbacks(0);

        // unlock while waiting for the device
        this->unlock();
        // call to this function will block and wait for device to
        // signal acquisition finished condition (interrupt or poll)
        // or timeout on while waiting (not an error!)
        ret = waitForDevice();
        // lock it back
        this->lock();
        if (ret) {
            // error occured while waiting for trigger or trigger did not arrive
            disarmDevice();

            // perform the register readout and update scalar PVs
            deviceDone();
            readbackParameters();
            snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "%s", "timeout while waiting for trigger");
            setStringParam(IFCDriverMessage, mMessageBuffer);

            // timeout while waiting for trigger is NOT an error
            if (ret != status_irq_timeout) {
                // leave acquiring state if there was an error!
                mAcquiring = 0;
                snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "%s", "error while waiting for trigger");
                setStringParam(IFCDriverMessage, mMessageBuffer);
            }
            for (int addr = 0; addr < maxAddr; addr++) {
                callParamCallbacks(addr);
            }

            // should this be set if an error happened?
            setIntegerParam(IFCTriggerTimeout, 1);
            goto taskStart;
        }

        // trigger arrived!
        setIntegerParam(IFCTriggerTimeout, 0);
        triggerCount++;

        // obtain the device generated updates in registers
        // usually read-only registers that are not touched by user with register writes
        ret = deviceDone();
        if (ret) {
            mAcquiring = 0;
            goto taskStart;
        }

        ret = acquireArrays();
        if (ret) {
            mAcquiring = 0;
            goto taskStart;
        }

        epicsTimeStamp frameTime;
        epicsTimeGetCurrent(&frameTime);
        getIntegerParam(NDArrayCounter, &arrayCounter);
        arrayCounter++;
        setIntegerParam(NDArrayCounter, arrayCounter);
        mUniqueId++;

        for (int addr = 0; addr < maxAddr; addr++) {
            callParamCallbacks(addr);
        }

        // notify clients of the new data arrays and time ticks for enabled
        // channels and AOIs
        for (int addr = 0; addr < maxAddr; addr++) {
            if (! this->pArrays[addr]) {
                continue;
            }
            pData = this->pArrays[addr];

            // set the frame number and time stamp
            pData->uniqueId = mUniqueId;
            pData->timeStamp = frameTime.secPastEpoch + frameTime.nsec / 1.e9;
            updateTimeStamp(&pData->epicsTS);

            // get any attributes that have been defined for this driver
            this->getAttributes(pData->pAttributeList);

            // must release the lock here, or we can get into a deadlock, because we can
            // block on the plugin lock, and the plugin can be calling us
            this->unlock();
            asynPrintDeviceInfo(pasynUserSelf, "performing doCallbacksGenericPointer for channel %d..", addr);
            doCallbacksGenericPointer(pData, NDArrayData, addr);
            this->lock();
        }

        getIntegerParam(IFCTriggerRepeat, &triggerRepeat);
        asynPrintDeviceInfo(pasynUserSelf, "trigger repeat=%d, trigger count=%d", triggerRepeat, triggerCount);
        // stop the acq if trigger is not set to repeat,
        // or the trigger count has been reached
        if ((triggerRepeat == 0) || ((triggerRepeat > 0) && (triggerCount >= triggerRepeat))) {
            asynPrintDeviceInfo(pasynUserSelf, "stopping acquisition, trigger count %d reached", triggerCount);
            mAcquiring = 0;
        }

        // /* Call the callbacks to update any changes */
        // for (i=0; i<maxAddr; i++) {
        //     callParamCallbacks(i);
        // }
    }

    callParamCallbacks(0);
    asynPrintDeviceInfo(pasynUserSelf, "data thread stop");
}


int ifc1400::updateParameters()
{
	struct timespec now = {0, 0};
	ifcdaqdrv_status status;

	clock_gettime(CLOCK_REALTIME, &now);

	uint32_t nchannels;
	uint32_t sample_size;
	status = ifcdaqdrv_get_nchannels(mIFCDevice, &nchannels);
	if (status) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_nchannels returned %d", status);
        return (int)status;
    }
    
    status = ifcdaqdrv_get_resolution(mIFCDevice, &sample_size);
	if (status) {
        asynPrintError(pasynUserSelf, "ifcdaqdrv_get_resolution returned %d", status);
        return (int)status;
    }
	sample_size /= 8;


	/* Local variables for managing sampling rate related parameters */
	int pAveraging;
	int pDecimation;
	int pClkDiv;
	double pClkFreq;
	double pSamplingRate;
	getIntegerParam(IFCClockDivider, &pClkDiv);
	getDoubleParam(IFCSamplingFrequency, &pSamplingRate);


	if(mDoNumSamplesUpdate) {
		mDoNumSamplesUpdate = false;

        uint32_t curr_nSamples, maxSamples;
        getIntegerParam(IFCNumSamples, (int *) &curr_nSamples);
        curr_nSamples = ceil_pow2(curr_nSamples);

        // get_nsamples will return max number of samples
        status = ifcdaqdrv_get_nsamples(mIFCDevice, &maxSamples);
    	if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_resolution returned %d", status);
            return (int)status;
        }
        if (curr_nSamples > maxSamples)
            curr_nSamples = maxSamples;

        // Assert a valid number of samples
        setIntegerParam(IFCNumSamples, (int) curr_nSamples);
	}

	if(mDoTriggerUpdate || mDoTriggerThresUpdate)
	{
		mDoTriggerUpdate = false;
		mDoTriggerThresUpdate = false;

		ifcdaqdrv_trigger_type triggerType = ifcdaqdrv_trigger_none;
		uint32_t threshold = 0;
		uint32_t mask = 0;
		uint32_t polarity = 0;

		/* Current parameters */
		int curr_TrigSource;
		//int curr_TrigEdge;
		//double curr_TrigThres;
		getIntegerParam(IFCTriggerSource, &curr_TrigSource);
		// getIntegerParam(mIFCTrigEdge, &curr_TrigEdge);
		// getDoubleParam(mIFCTrigThres, &curr_TrigThres);

		switch (curr_TrigSource)
		{
            case 0: // Software
                triggerType = ifcdaqdrv_trigger_soft;
                break;
            /* From 1 to 8 is MTCA backplane trigger */
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                /* Use variable mask to specify backplane line */
                mask = curr_TrigSource - 1; // 0 to 7
                triggerType = ifcdaqdrv_trigger_backplane;
                break;
            default:
                triggerType = ifcdaqdrv_trigger_soft;
                break;
		}

		status = ifcdaqdrv_set_trigger(mIFCDevice, triggerType, threshold, mask, polarity);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_trigger returned %d", status);
            return (int)status;
        }
        status = ifcdaqdrv_set_trigger(mIFCDevice2, triggerType, threshold, mask, polarity);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_trigger returned %d", status);
            return (int)status;
        }

		if(!status) {
			//setDoubleParam(mIFCTrigThres, curr_TrigThres);
			setIntegerParam(IFCTriggerSource, curr_TrigSource);
			//setIntegerParam(mIFCTrigEdge, curr_TrigEdge);
		}
	}

	if(mDoClockSrcUpdate) {
		mDoClockSrcUpdate = false;

		int clkSource;
        //TODO: removed forced INTERNAL clock
		//getIntegerParam(IFCClockSource, &clkSource);
        clkSource = 0; //ifcdaqdrv_clock_internal

		status = ifcdaqdrv_set_clock_source(mIFCDevice, (ifcdaqdrv_clock)clkSource);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_source returned %d", status);
            return (int)status;
        }
        status = ifcdaqdrv_set_clock_source(mIFCDevice2, (ifcdaqdrv_clock)clkSource);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_source returned %d", status);
            return (int)status;
        }

		status = ifcdaqdrv_get_clock_source(mIFCDevice, (ifcdaqdrv_clock*) &clkSource);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_source returned %d", status);
            return (int)status;
        }
        status = ifcdaqdrv_get_clock_source(mIFCDevice2, (ifcdaqdrv_clock*) &clkSource);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_source returned %d", status);
            return (int)status;
        }
		setIntegerParam(IFCClockSource, clkSource);
	}

	if(mDoClockFreqUpdate) {
		mDoClockFreqUpdate = false;

		status = ifcdaqdrv_set_clock_frequency(mIFCDevice, pClkFreq);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_frequency returned %d", status);
            return (int)status;
        }
        status = ifcdaqdrv_set_clock_frequency(mIFCDevice2, pClkFreq);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_frequency returned %d", status);
            return (int)status;
        }

		status = ifcdaqdrv_get_clock_frequency(mIFCDevice, &pClkFreq);
        status = ifcdaqdrv_get_clock_frequency(mIFCDevice, &pClkFreq);
		if(status) {
			asynPrintError(pasynUserSelf, "ifcdaqdrv_get_clock_frequency returned %d", status);
            return (int)status;
		} else {
			// Update sampling rate
			ifcdaqdrv_calc_sample_rate(mIFCDevice, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 0);
            ifcdaqdrv_calc_sample_rate(mIFCDevice2, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 0);
			setDoubleParam(IFCSamplingFrequency, pSamplingRate);
		}
		//setDoubleParam(mIFCClockFreq, pClkFreq);
	}

	if(mDoClockDivUpdate) {
		mDoClockDivUpdate = false;

        //TODO: allow change of clock
		// status = ifcdaqdrv_set_clock_divisor(mIFCDevice, (uint32_t) pClkDiv);
        // if (status) {
        //     asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_divisor returned %d", status);
        //     return (int)status;
        // }
        // status = ifcdaqdrv_set_clock_divisor(mIFCDevice2, (uint32_t) pClkDiv);
		// if (status) {
        //     asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_divisor returned %d", status);
        //     return (int)status;
        // }

		status = ifcdaqdrv_get_clock_divisor(mIFCDevice, (uint32_t*) &pClkDiv);
		if(status) {
			asynPrintError(pasynUserSelf, "ifcdaqdrv_set_clock_divisor returned %d", status);
			pClkDiv = 0;
            return (int)status;
		} else {
			// Update sampling rate
			ifcdaqdrv_calc_sample_rate(mIFCDevice, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 0);
            ifcdaqdrv_calc_sample_rate(mIFCDevice2, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 0);
			setDoubleParam(IFCSamplingFrequency, pSamplingRate);
		}
		setIntegerParam(IFCClockDivider, pClkDiv);
	}

	if(mDoSamplingRateUpdate) {
		mDoSamplingRateUpdate = false;

		status = ifcdaqdrv_calc_sample_rate(mIFCDevice, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 1);
		if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_calc_sample_rate returned %d", status);
            return (int)status;
        }
        status = ifcdaqdrv_calc_sample_rate(mIFCDevice2, (int32_t*)&pAveraging, (int32_t*)&pDecimation, (int32_t*)&pClkDiv, &pClkFreq, &pSamplingRate, 1);
		if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_calc_sample_rate returned %d", status);
            return (int)status;
        }

		setIntegerParam(IFCClockDivider, pClkDiv);
		setDoubleParam(IFCSamplingFrequency, pSamplingRate);
	}

        /* Enable / disable channels. */
    if (mDoChannelMaskUpdate) {
        for (uint32_t a = 0; a < mNumChannels; a++) {
            status = (ifcdaqdrv_status) setupChannelMask(a);
        }
        mDoChannelMaskUpdate = false;
    }

    updateTickValue();

	return 0;
}

int ifc1400::updateParameters(int addr) {
	ifcdaqdrv_status status = status_success;
    
    if(mDoChannelGainUpdate) {
		mDoNumSamplesUpdate = false;

        int gain_;
        getIntegerParam(addr, IFCChGain, &gain_);

        if ((gain_ != 1) && (gain_ != 2) && (gain_ != 5) && (gain_ != 10))
            gain_ = 1;
        
        
        if (addr < 20)
            status = ifcdaqdrv_set_gain(mIFCDevice, addr, (double)gain_);
        else
            status = ifcdaqdrv_set_gain(mIFCDevice2, (addr-20), (double)gain_);
		
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_set_gain returned %d", status);
            return (int)status;
        }

        // set configuration in ADC3117
        if (addr < 20)
            status = ifcdaqdrv_send_configuration_command(mIFCDevice);
        else 
            status = ifcdaqdrv_send_configuration_command(mIFCDevice2);
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_send_configuration_command returned %d", status);
            return (int)status;
        }
        
        //readback
        double gaind_;
        if (addr < 20)
            status = ifcdaqdrv_get_gain(mIFCDevice, addr, &gaind_);
        else
            status = ifcdaqdrv_get_gain(mIFCDevice2, (addr-20), &gaind_);
		
        if (status) {
            asynPrintError(pasynUserSelf, "ifcdaqdrv_get_gain returned %d", status);
            return (int)status;
        }
        setIntegerParam(addr, IFCChGain, static_cast<int>(gaind_));
	}

    if (mDoInputChannelsUpdate) {
        mDoInputChannelsUpdate = false;
        epicsInt32 posinput, neginput;
        getIntegerParam(addr, IFCChPosInput, &posinput);
        getIntegerParam(addr, IFCChNegInput, &neginput);

        printf("pos input val = %d\n", posinput);
        printf("neg input val = %d\n", neginput);

        if (addr < 20 ) {
            status = ifcdaqdrv_set_adc_channel_positive_input(mIFCDevice, addr, (uint8_t)posinput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_set_adc_channel_positive_input returned %d", status);
                return (int)status;
            }
            status = ifcdaqdrv_set_adc_channel_negative_input(mIFCDevice, addr, (uint8_t)neginput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_set_adc_channel_negative_input returned %d", status);
                return (int)status;
            }

           	ifcdaqdrv_send_configuration_command(mIFCDevice);

            /* Asserting the parameters */
            status = ifcdaqdrv_get_adc_channel_positive_input(mIFCDevice, addr, (uint8_t*)&posinput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_get_adc_channel_positive_input returned %d", status);
                return (int)status;
            }
            status = ifcdaqdrv_get_adc_channel_negative_input(mIFCDevice, addr, (uint8_t*)&neginput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_get_adc_channel_negative_input returned %d", status);
                return (int)status;
            }
        } else {
            status = ifcdaqdrv_set_adc_channel_positive_input(mIFCDevice2, (addr-20), (uint8_t)posinput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_set_adc_channel_positive_input returned %d", status);
                return (int)status;
            }
            status = ifcdaqdrv_set_adc_channel_negative_input(mIFCDevice2, (addr-20), (uint8_t)neginput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_set_adc_channel_negative_input returned %d", status);
                return (int)status;
            }

            ifcdaqdrv_send_configuration_command(mIFCDevice2);

            /* Asserting the parameters */
            status = ifcdaqdrv_get_adc_channel_positive_input(mIFCDevice2, (addr-20), (uint8_t*)&posinput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_get_adc_channel_positive_input returned %d", status);
                return (int)status;
            }
            status = ifcdaqdrv_get_adc_channel_negative_input(mIFCDevice2, (addr-20), (uint8_t*)&neginput);
            if (status) {
                asynPrintError(pasynUserSelf, "ifcdaqdrv_get_adc_channel_negative_input returned %d", status);
                return (int)status;
            }
            
        }
        setIntegerParam(addr, IFCChPosInput, posinput);
        setIntegerParam(addr, IFCChNegInput, neginput);    
    }
    return 0;
}


/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus ifc1400::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr;
    int ret;
    const char *name;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %d", name, function, value);
    status = setIntegerParam(addr, function, value);

    if (function == ADAcquire) {
        setAcquire(value);
    }

    else if (function == IFCNumSamples) {
        mDoNumSamplesUpdate = true;
    }

    else if (function == IFCTriggerSource) {
        mDoTriggerUpdate = true;
    }

    else if (function == IFCClockSource) {
        mDoClockSrcUpdate = true;
    }

    else if (function == IFCClockDivider) {
        mDoClockDivUpdate = true;
    }

    else if (function == IFCChControl) {
        mDoChannelMaskUpdate = true;
    }

    else if ((function == IFCChPosInput) || (function == IFCChNegInput)) {
        printf("addr is %d value is %d\n", addr, value);
        mDoInputChannelsUpdate = true;
    }

    else if (function == IFCRtmType) {
        // No RTM support at the moment, force to NONE
        setIntegerParam(IFCRtmType, 0);
    }

    else if (function == IFCChGain) {
        mDoChannelGainUpdate = true;
    }

    /* If this parameter belongs to a base class call its method */
    if (function < IFC_FIRST_PARAM) {
        status = asynNDArrayDriver::writeInt32(pasynUser, value);
    }

    /* UPDATE global parameters on the firmware */
    ret = updateParameters();
    if (ret) {
        status = asynError;
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %d", portName, addr, name, value);
        setStringParam(IFCDriverMessage, mMessageBuffer);
    }

    /* Update channel parameters */
    ret = updateParameters(addr);
    if (ret) {
        status = asynError;
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %d", portName, addr, name, value);
        setStringParam(IFCDriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%d", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%d", status, function, value);
    }

    // read back the firmware register values
    readbackParameters();

    return status;
}


/** Called when asyn clients call pasynFloat64->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus ifc1400::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int addr;
    const char *name;
    int ret = 0;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %f", name, function, value);

    status = setDoubleParam(addr, function, value);

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setDoubleParam(addr, function, value);

    /* If it's changing the sampling frequency */
    if (function == IFCSamplingFrequency) {
        mDoSamplingRateUpdate = true;
    }

    /* If it's changing the trigger threashold */
    // else if (function == mIFCTrigThres) {
    //     mDoTriggerThresUpdate = true;
    // }

    // /* If it's changing the clock frequency */
    // else if (function == mIFCClockFreq) {
    //     mDoClockFreqUpdate = true;
    // }


    /* If this parameter belongs to a base class call its method */
    if (function < IFC_FIRST_PARAM) {
        status = asynNDArrayDriver::writeFloat64(pasynUser, value);
    }

    /* UPDATE parameters on the firmware */
    ret = updateParameters();
    if (ret) {
        status = asynError;
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %f", portName, addr, name, value);
        setStringParam(IFCDriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%f", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%f", status, function, value);
    }

    // read back the firmware register values
    // XXX is ignoring the errors here OK?!
    readbackParameters();
    return status;
}

// called when asyn clients call pasynOctet->write()
asynStatus ifc1400::writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual)
{
    int addr;
    int function = pasynUser->reason;
    const char *name;
    asynStatus status = asynSuccess;
    int ret = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %s", name, function, value);

    status = setStringParam(addr, function, (char *)value);

    if (function == IFCEvrTimestamp) {
        // remember parsed timestamp values
        ret = sscanf(value, "%u.%u", &mTimeStampSec, &mTimeStampNsec);
        if (ret == 2) {
            // convert to EPICS time (epoch at 1990)
            mTimeStampSec -= POSIX_TIME_AT_EPICS_EPOCH;
            asynPrintDriverInfo(pasynUser, "EPICS time stamp: %u sec %u nsec", mTimeStampSec, mTimeStampNsec);
            ret = 0;
        } else {
            asynPrintError(pasynUser, "sscanf of timestamp returned %d", ret);
            ret = -1;
        }
    } else {
        if (function < IFC_FIRST_PARAM) {
            status = asynNDArrayDriver::writeOctet(pasynUser, value, nChars, nActual);
        }
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %s", portName, addr, name, value);
        setStringParam(IFCDriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%s", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%s", status, function, value);
    }

    // *not* reading back the firmware register values
    // readbackParameters();

    *nActual = nChars;
    return status;
}

/** Report status of the driver.
  * Prints details about the driver if details>0.
  * It then calls the ADDriver::report() method.
  * \param[in] fp File pointed passed by caller where the output is written to.
  * \param[in] details If >0 then driver details are printed.
  */
void ifc1400::report(FILE *fp, int details)
{
    int deviceType;
    int APPSinature;
    char buf[256];
    
    fprintf(fp, "Port name        : %s\n", this->portName);
    getStringParam(IFCDevicePath, 256, buf);
    fprintf(fp, "Device path      : %s\n", buf);
    getStringParam(ADManufacturer, 256, buf);
    fprintf(fp, "Manufacturer     : %s\n", buf);
    getStringParam(ADFirmwareVersion, 256, buf);
    fprintf(fp, "Firmware version : %s\n", buf);
    getStringParam(ADSerialNumber, 256, buf);
    fprintf(fp, "Serial number    : %s\n", buf);
    getStringParam(ADModel, 256, buf);
    fprintf(fp, "Model            : %s\n", buf);

    // getIntegerParam(IFCMemorySize, &memorySizeMb);
    // fprintf(fp, "Memory size      : %d MB\n", memorySizeMb);

    getIntegerParam(IFCAPPSinature, &APPSinature);
    getIntegerParam(IFCDeviceType, &deviceType);
    fprintf(fp,
            "Device type      : %X\n"
            "Firmware version : 0x%4X\n",
            deviceType,
            APPSinature);
    if (details > 0) {
        int numSamples, dataType;
        getIntegerParam(IFCNumSamples, &numSamples);
        getIntegerParam(NDDataType, &dataType);
        fprintf(fp, "  # samples:       %d\n", numSamples);
        fprintf(fp, "      Data type:   %d\n", dataType);
    }
    /* Invoke the base class method */
    asynNDArrayDriver::report(fp, details);
}


/*********************************************************************************************************************/

/** Configuration command, called directly or from iocsh */
extern "C" int ifc1400Config(const char *portName, const int ifccard, const int maxChannels,
        int numSamples, int extraPorts, int maxBuffers, int maxMemory,
        int priority, int stackSize)
{
    new ifc1400(portName, ifccard, maxChannels,
            numSamples, extraPorts,
            (maxBuffers < 0) ? 0 : maxBuffers,
            (maxMemory < 0) ? 0 : maxMemory,
            priority, stackSize);
    return(asynSuccess);
}

/** Code for iocsh registration */
static const iocshArg arg0 = {"Port name",     iocshArgString};
static const iocshArg arg1 = {"IFC Card",      iocshArgInt};
static const iocshArg arg2 = {"Max Channels",  iocshArgInt};
static const iocshArg arg3 = {"Num samples",   iocshArgInt};
static const iocshArg arg4 = {"Extra ports",   iocshArgInt};
static const iocshArg arg5 = {"maxBuffers",    iocshArgInt};
static const iocshArg arg6 = {"maxMemory",     iocshArgInt};
static const iocshArg arg7 = {"priority",      iocshArgInt};
static const iocshArg arg8 = {"stackSize",     iocshArgInt};
static const iocshArg * const configArgs[] = {
    &arg0, &arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7, &arg8};
static const iocshFuncDef config = {"ifc1400Configure", 9, configArgs};
static void configCallFunc(const iocshArgBuf *args)
{
    ifc1400Config(args[0].sval, args[1].ival, args[2].ival,
        args[3].ival, args[4].ival, args[5].ival, args[6].ival, args[7].ival, args[8].ival);
}

static void ifc1400Register(void)
{
    iocshRegister(&config, configCallFunc);
}

extern "C" {
epicsExportRegistrar(ifc1400Register);
}
