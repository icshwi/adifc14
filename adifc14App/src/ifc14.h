/* ifc14.h
 *
 * This is a driver for a IOxOS IFC1410 board with ADC311X digitizers FMCs.
 * Based on ADSIS8300 from Hinko Kocevar (ESS ERIC).
 *
 * Author: Joao Paulo Martins
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  January 03, 2019
 *
 */

#ifndef _IFC14_H_
#define _IFC14_H_

#include <stdint.h>
#include <epicsEvent.h>
#include <epicsTime.h>

//#include <asynDriver.h>
#include <asynNDArrayDriver.h>

#include "ifcdaqdrv.h"

/* Compilation parameters */
//#define FAKEIFCDEV
#define ADC3110_NUM_CHANNELS            8
#define ADC3117_NUM_CHANNELS            20
#define MAX_PATH_LENGTH                 32
#define MAX_MESSAGE_LENGTH              256
#define MAX_LOG_STR_LEN                 256

/* asyn parameters strings */
// board strings
#define IFCDriverMessageString                      "IFC.DRIVER_MESSAGE"
#define IFCNumSamplesString                         "IFC.NUM_SAMPLES"
#define IFCClockSourceString                        "IFC.CLOCK_SOURCE"
#define IFCClockDividerString                       "IFC.CLOCK_DIVIDER"
#define IFCTriggerSourceString                      "IFC.TRIGGER_SOURCE"
#define IFCTriggerLineString                        "IFC.TRIGGER_EXTERNAL_LINE"
#define IFCTriggerDelayString                       "IFC.TRIGGER_DELAY"
#define IFCTriggerRepeatString                      "IFC.TRIGGER_REPEAT"
#define IFCIrqPollString                            "IFC.IRQ_POLL"
#define IFCResetString                              "IFC.RESET"
#define IFCDevicePathString                         "IFC.DEVICE_PATH"
#define IFCMemorySizeString                         "IFC.MEMORY_SIZE"
#define IFCRtmTypeString                            "IFC.RTM_TYPE"
#define IFCSamplingFrequencyString                  "IFC.SAMPLING_FREQUENCY"
#define IFCTickValueString                          "IFC.TICK_VALUE"
#define IFCBeamModeSourceString                     "IFC.BEAM_MODE.SOURCE"
#define IFCBeamModeValueString                      "IFC.BEAM_MODE.VALUE"
#define IFCBeamDestinationSourceString              "IFC.BEAM_DESTINATION.SOURCE"
#define IFCBeamDestinationValueString               "IFC.BEAM_DESTINATION.VALUE"
#define IFCPcieLinkSpeedString                      "IFC.PCIE_LINK_SPEED"
#define IFCPcieLinkWidthString                      "IFC.PCIE_LINK_WIDTH"
#define IFCPcieTransferTimeString                   "IFC.PCIE_TRANSFER_TIME"
#define IFCPcieStartTimeString                      "IFC.PCIE_START_TIME"
#define IFCPcieEndTimeString                        "IFC.PCIE_END_TIME"
#define IFCTransferArraysString                     "IFC.TRANSFER_ARRAYS"
#define IFCTriggerTimeoutString                     "IFC.TRIGGER_TIMEOUT"
#define IFCTriggerTimeoutValueString                "IFC.TRIGGER_TIMEOUT_VALUE"
#define IFCAPPSinatureString                        "IFC.APP_SIGN"
#define IFCFWSignatureString                        "IFC.FW_SIGN"
#define IFCDeviceTypeString                         "IFC.DEVICE_TYPE"
#define IFCDeviceTypeStrString                      "IFC.DEVICE_TYPE_STR"

// EVR related strings
#define IFCEvrTimestampString                       "IFC.EVR.TIMESTAMP"
#define IFCEvrBeamStateString                       "IFC.EVR.BEAM_STATE"
#define IFCEvrLinkString                            "IFC.EVR.LINK"
#define IFCEvrLinkStatusString                      "IFC.EVR.LINK_STATUS"
#define IFCEvrLinkSeverityString                    "IFC.EVR.LINK_SEVERITY"
// channel strings
#define IFCChControlString                          "IFC.CH.CONTROL"
#define IFCChAttenuationString                      "IFC.CH.ATTENUATION"
#define IFCChInternalTriggerLengthString            "IFC.CH.INTERNAL_TRIGGER_LENGTH"
#define IFCChInternalTriggerConditionString         "IFC.CH.INTERNAL_TRIGGER_CONDITION"
#define IFCChInternalTriggerOffString               "IFC.CH.INTERNAL_TRIGGER_OFF"
#define IFCChInternalTriggerOnString                "IFC.CH.INTERNAL_TRIGGER_ON"
#define IFCChPcieTransferTimeString                 "IFC.CH.PCIE_TRANSFER_TIME"
#define IFCChPcieStartTimeString                    "IFC.CH.PCIE_START_TIME"
#define IFCChPosInputString                         "IFC.CH.POSINPUT"
#define IFCChNegInputString                         "IFC.CH.NEGINPUT"
#define IFCChGainString                             "IFC.CH.GAIN"

#define asynPrintError(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACE_ERROR, \
            "%s::%s [ERROR] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintDeviceInfo(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACEIO_DEVICE, \
            "%s::%s [INFO] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintDriverInfo(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, \
            "%s::%s [INFO] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintFlow(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACE_FLOW, \
            "%s::%s [FLOW] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

typedef enum {
              FROM_CONNECTOR,
              GND,
              OFFSET_COMP_VOLTAGE,
              VCAL
} ADC3117_INPUT_FUNCTION;


/** IFC14xx driver class **/
class epicsShareClass ifc1400 : public asynNDArrayDriver {
public:
    ifc1400(const char *portName, const int ifccard, const int maxChannels,
            int numSamples, int extraPorts, int maxBuffers, size_t maxMemory,
            int priority, int stackSize);
    virtual ~ifc1400();

    /* These are the methods that we override from asynNDArrayDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);
    virtual void report(FILE *fp, int details);
    /**< Should be private, but gets called from C, so must be public */
    void ifcTask();

protected:
    // board parameters
    int IFCNumSamples;
    #define IFC_FIRST_PARAM IFCNumSamples
    int IFCClockSource;
    int IFCClockDivider;
    int IFCTriggerSource;
    int IFCTriggerExternalLine;
    int IFCTriggerDelay;
    int IFCTriggerRepeat;
    int IFCIrqPoll;
    int IFCReset;
    int IFCDriverMessage;
    int IFCDevicePath;
    int IFCMemorySize;
    int IFCRtmType;
    int IFCSamplingFrequency;
    int IFCTickValue;
    int IFCBeamModeSource;
    int IFCBeamModeValue;
    int IFCBeamDestinationSource;
    int IFCBeamDestinationValue;
    int IFCPcieLinkSpeed;
    int IFCPcieLinkWidth;
    int IFCPcieTransferTime;
    int IFCPcieStartTime;
    int IFCPcieEndTime;
    int IFCTransferArrays;
    int IFCTriggerTimeout;
    int IFCTriggerTimeoutValue;
    int IFCAPPSinature;
    int IFCFWSignature;
    int IFCDeviceType;
    int IFCDeviceTypeStr;

    // EVR parameters
    int IFCEvrTimestamp;
    int IFCEvrBeamState;
    int IFCEvrLink;
    int IFCEvrLinkStatus;
    int IFCEvrLinkSeverity;
    // channel parameters
    int IFCChControl;
    int IFCChAttenuation;
    int IFCChInternalTriggerLength;
    int IFCChInternalTriggerCondition;
    int IFCChInternalTriggerOff;
    int IFCChInternalTriggerOn;
    int IFCChPcieTransferTime;
    int IFCChPcieStartTime;
    int IFCChPosInput;
    int IFCChNegInput;
    int IFCChGain; 

    // these are the methods that are new to this class
    int acquireRawArrays();
    template <typename epicsType> int convertArraysT();
    virtual int acquireArrays();
    void setAcquire(int value);
    virtual int initDevice();
    virtual int destroyDevice();
    virtual int initDeviceDone();
    virtual int armDevice();
    virtual int disarmDevice();
    virtual int waitForDevice();
    virtual int deviceDone();
    virtual int readbackParameters();
    virtual int setupChannelMask(int addr);
    virtual int setupInternalTrigger(int addr);
    virtual void updateTickValue();
    virtual bool checkEvrLink();
    virtual int updateParameters();
    virtual int updateParameters(int addr);
    
    struct ifcdaqdrv_usr *mIFCDevice;
    struct ifcdaqdrv_usr *mIFCDevice2;
    char mMessageBuffer[MAX_MESSAGE_LENGTH];

    uint64_t mChannelMask;
    uint32_t mNumChannels;
    char mIFCLogStr[MAX_LOG_STR_LEN];
    NDArray *mRawDataArray;
    int mNumArrays;

private:
    /* Our data */
    epicsEventId mStartEventId;
    epicsEventId mStopEventId;
    int mUniqueId;
    int mAcquiring;
    char mDevicePath[MAX_PATH_LENGTH];



    unsigned int mIFCFirmwareOptions;
    bool mDoNumSamplesUpdate;
    bool mDoTriggerUpdate;
    bool mDoTriggerThresUpdate;
    bool mDoClockSrcUpdate;
    bool mDoClockFreqUpdate;
    bool mDoClockDivUpdate;
    bool mDoDecimationUpdate;
    bool mDoAveragingUpdate;
    bool mDoSamplingRateUpdate;
    bool mDoChannelMaskUpdate;
    bool mDoInputChannelsUpdate;
    bool mDoChannelGainUpdate;

public:
    // these two should be protected but need to be public since called from
    // myTimeStampSource()
    epicsUInt32 mTimeStampSec;
    epicsUInt32 mTimeStampNsec;
    epicsTimeStamp mInterruptTs;


};



#endif /* _IFC14_H_ */
